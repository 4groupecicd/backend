# This file is a template, and might need editing before it works on your project.
FROM python:3.10.7

WORKDIR /backend
COPY requirements.txt .
RUN pip3 install -r requirements.txt

COPY . .

# For Django
EXPOSE 8000
#CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]

# For some other command
RUN chmod u+x ./start.sh
 CMD ["./start.sh"]
